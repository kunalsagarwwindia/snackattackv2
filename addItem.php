<?php
    session_start();
    $success_msg = "";
    $error_msg = "";
    if(!isset($_SESSION['user']))
        header("location:index.php");
    require_once 'db.php';    
    require_once 'header.php';    
    
    if(isset($_POST['add_item'])){
        $item = strip_tags($_POST['item']);
        $price = strip_tags($_POST['price']);
        
        if(!is_numeric($price)){
            $error_msg .= "Enter valid Price! ";
        } else if(empty($item)){
            $error_msg .= "Enter Item name ";
        }else {
            $insertQuery = "INSERT into sa_items(item, price, user_id) value('".$item."', ".$price.", ".$user->id.")";
            mysql_query($insertQuery, $link) or die('Unable to add product');
            $success_msg = "Item created successfully";
        }
    }
    
?>
    <?php
        if(!empty($error_msg)){
    ?>
    <div class="error"><?php echo $error_msg; ?></div>
    <?php
        }
    ?>
    
    <?php
        if(!empty($success_msg)){
    ?>
    <div class="success"><?php echo $success_msg; ?></div>
    <?php
        }
    ?>
    
    

    <br /><br />Add Item<br /><br />
    <form action="addItem.php" method="post">
        Item name : <input type="text" name="item" /><br />
        Price : <input type="text" name="price" /><br /><br />
        <input type="submit" name="add_item" value="Add Item" />        
    </form>

<?php require_once 'footer.php'; ?>