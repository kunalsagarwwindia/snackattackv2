-- MySQL dump 10.13  Distrib 5.5.24, for Win32 (x86)
--
-- Host: localhost    Database: snackAttack
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sa_items`
--

DROP TABLE IF EXISTS `sa_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sa_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(999) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `status` enum('enable','delete') DEFAULT 'enable',
  `user_id` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sa_items`
--

LOCK TABLES `sa_items` WRITE;
/*!40000 ALTER TABLE `sa_items` DISABLE KEYS */;
INSERT INTO `sa_items` VALUES (1,'Veg Sandwich',15,'enable',1),(2,'Veg Toast Sandwich',17,'enable',1),(3,'Veg Grill sandwich',35,'enable',1),(4,'veg pizza',44,'enable',8),(5,'xyz',123,'enable',13),(6,'Masala Grill S/w',35,'enable',5),(7,'Non-Veg Pizza',70,'enable',12),(8,'123',123,'enable',13),(9,'Mayonnaise Malsala Wrap',35,'enable',12),(10,'Mc Chicken Burger',59,'enable',12),(11,'Mc Veggie Burger',49,'enable',12),(12,'Tandoori Chicken Full',190,'enable',12);
/*!40000 ALTER TABLE `sa_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sa_orders`
--

DROP TABLE IF EXISTS `sa_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sa_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status` enum('enable','delete') DEFAULT 'enable',
  `order_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `qty` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sa_orders`
--

LOCK TABLES `sa_orders` WRITE;
/*!40000 ALTER TABLE `sa_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `sa_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sa_users`
--

DROP TABLE IF EXISTS `sa_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sa_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(999) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','delete','ban') DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sa_users`
--

LOCK TABLES `sa_users` WRITE;
/*!40000 ALTER TABLE `sa_users` DISABLE KEYS */;
INSERT INTO `sa_users` VALUES (1,'Kunal Sagar','kunal.sagar@wwindia.com','P@ssw0rd123','2012-06-19 13:36:57','active'),(2,'sudik','sudik.maharana@wwindia.com','sudik','2012-06-20 11:11:33','active'),(3,'Snehal Penurkar','snehal.penurkar@wwindia.com','123swati','2012-06-20 11:12:05','active'),(4,'Dhara Jogi','dhara.jogi@wwindia.com','dharaj123','2012-06-20 11:12:21','active'),(5,'Sagar Tandel','sagar.tandel@wwindia.com','sagart123','2012-06-20 11:26:41','active'),(6,'pushan','pushan@wwindia.com','pushan','2012-06-20 11:43:45','active'),(7,'Mayur','mayuresh.gatare@wwindia.com','mayurg555','2012-06-20 11:44:24','active'),(8,'venkatesh dayawar','venkatesh.webwerks@gmail.com','webwerks1987','2012-06-20 11:44:42','active'),(9,'mona','monali.deshmukh@wwindia.com','monalid123','2012-06-20 11:44:49','active'),(10,'Jignesh Patel','jignesh.patel@wwindia.com','password','2012-06-20 11:46:19','active'),(11,'Sonusingh Chauhan','sonusingh.chauhan@wwindia.com','112855','2012-06-20 11:46:24','active'),(12,'piyush','piyush.bobhate@wwindia.com','123piyush','2012-06-20 11:46:55','active'),(13,'abc','abc@abc.com','abc','2012-06-20 11:47:12',''),(14,'rakesh','rakesh.rapelliwar@wwindia.com','rakesh','2012-06-20 11:47:54','active'),(15,'Dattatraya','dattatraya.barade@wwindia.com','datta123','2012-06-20 11:48:52','active'),(16,'testing','','testing','2012-06-20 11:51:09','active');
/*!40000 ALTER TABLE `sa_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-06-22 13:51:29
