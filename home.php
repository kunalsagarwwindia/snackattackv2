<?php
    session_start();
    $success_msg = "";
    if(!isset($_SESSION['user']))
        header("location:index.php");
    require_once 'db.php';    
    require_once 'header.php';
    
    if(isset($_POST['place_order'])){
        $item = $_POST['item'];
        $qty = $_POST['qty'];
        placeOrderById($item, $qty, $user->id);
        $success_msg = "Order Placed Successfully.";
    }
    
?>

    <br /><br />Place Order<br /><br />
    <?php
        if(!empty($success_msg)){
    ?>
    <div class="success"><?php echo $success_msg; ?></div>
    <?php
        }
    ?>
    <form action="home.php" method="post">
    Select Item : 
        <select name="item">        
        <?php
            $itemsResult = getItemList();
            while($row = mysql_fetch_object($itemsResult)){
        ?>
            <option value ="<?php echo $row->id; ?>"><?php echo $row->item . ' -  Rs.'.$row->price; ?></option>
        <?php
            }
        ?>
        </select><br />
        Qty : 
        <select name="qty">
            <?php for($i = 1; $i < 11; $i++) { ?>
            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
        </select><br /><br />
        <input type="submit" name="place_order" value="Place Order" />
    </form>

    <!-- Today's Order -->
    <br /><br />
    Yours Today's Order<br /><br />
    <table cellpadding="4">
        <tr>
            <th>#</th>
            <th>Item</th>
            <th>Qty</th>
            <th>Total Rupees</th>
        </tr>
    <?php 
        $resultTodaysOrder = getTodaysOrderById($user->id);        
        $i = 1;
        $grandTotal = 0;
        while($row = mysql_fetch_row($resultTodaysOrder)){
    ?>
        <tr>
            <td align="CENTER"><?php echo $i ?></td>
            <td align="LEFT"><?php echo $row[0]; ?></td>
            <td align="CENTER"><?php echo $row[1]; ?></td>
            <td align="CENTER"><?php echo $row[2]; $grandTotal+=$row[2]; ?></td>
        </tr>
    <?php $i++; } ?>
        <tr>
            <td colspan="3" align="CENTER"><b>Grand Total</b></td>
            <td align="CENTER"><b><?php echo $grandTotal; ?></b></td>
        </tr>
    </table>
    
<?php require_once 'footer.php'; ?>
